//
//  ViewController.swift
//  String
//
//  Created by Nikita Nechyporenko on 25.07.18.
//  Copyright © 2018 Nikita Nechyporenko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        quantitySimbolsOfName()
        //        checkIchNa()
        //        splitName()
        //        reverce(startWord: "Ось")
        //        addPoint(number: "1111111111111111")
        //        search(letters: "da")
        //        checkPassword(password: "12345qwerty")
        //            sortArr()
        //        convertStrToTranslite(strTranslite: "Карандаш")
    }
    
    func convertStrToTranslite(strTranslite: String) {
        var str = Array(strTranslite)
        var strTransliteRady = String()
        let myDictionary = ["а":"a","б":"b","в":"v","г":"g","д":"d","е":"e","ж":"g",
                            "з":"z","и":"i","й":"y","к":"k","л":"l","м":"m","н":"n",
                            "о":"o","п":"p","р":"r","с":"s","т":"t","у":"u","ф":"f",
                            "х":"h","ц":"c","ч":"ch","ш":"sh","щ":"sh`","ъ":"","ы":"i",
                            "ь":"","э":"e","ю":"yu","я":"ya","А":"A","Б":"B","В":"V",
                            "Г":"G","Д":"D","Е":"E","Ж":"G","З":"Z","И":"I","Й":"Y",
                            "К":"K","Л":"L","М":"M","Н":"N","О":"O","П":"P","Р":"R",
                            "С":"S","Т":"T","У":"U","Ф":"F","Х":"H","Ц":"C","Ч":"CH",
                            "Ш":"SH","Щ":"SH`","Ъ":"","Ы":"I","Ь":"","Э":"E","Ю":"YU","Я":"YA"]
        for _ in 0..<str.count{
            var firstLetterInStrTranslite = str.removeFirst()
            let letterToTranslite = String(firstLetterInStrTranslite)
            let translitedLetter = myDictionary[letterToTranslite] as! String
            strTransliteRady.append(translitedLetter)
        }
        print("translite \(strTranslite) -> \(strTransliteRady)")
    }
    
    func sortArr() {
        var arrToSortAndRemoveDuplicates = [Int]()//Array<Int>.init()//[Int]()//[Int].init()
        var arrOnlySorted = [Int]()
        var arrSortedAndREmovedDublicates = [Int]()
        arrToSortAndRemoveDuplicates.append(5)
        for _ in 0..<20 {
            arrToSortAndRemoveDuplicates.append(Int(arc4random()%10))
        }
        print(arrToSortAndRemoveDuplicates)
        arrToSortAndRemoveDuplicates.forEach { (numb) in
            if arrOnlySorted.contains(numb) {
                print("remove dublicate")
            }else{
                arrOnlySorted.append(numb)
            }
        }
        print(arrOnlySorted)
        var numberForCicle = -1
        for _ in 0..<10 {
            numberForCicle = numberForCicle + 1
            if arrOnlySorted.contains(numberForCicle) {
                arrSortedAndREmovedDublicates.append(numberForCicle)
            }
        }
        print(arrSortedAndREmovedDublicates)
    }
    
    func checkPassword(password: String) {
        let checkString = password
        var passwordCharacterArr = Array(checkString)
        let strAlphabet = "abcdefghijklmnopqrstuvwxyz"
        let strBigLettersAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        let strNumbers = "1234567890"
        var result = ""
        var resultStr = ""
        var resultInt = 0
        for _ in passwordCharacterArr {
            let firstLetter = passwordCharacterArr.removeFirst()
            if strNumbers.contains(firstLetter){
                result.append("a")
            }else
                if strBigLettersAlphabet.contains(firstLetter){
                    result.append("b")
                }else
                    if strAlphabet.contains(firstLetter){
                        result.append("c")
                    }else{
                        result.append("d")
            }
        }
        if result.contains("a") {
            resultStr.append(" a)")
            resultInt = 1
        }
        if result.contains("b") {
            resultStr.append(" b)")
            resultInt = resultInt + 1
        }
        if result.contains("c") {
            resultStr.append(" c)")
            resultInt = resultInt + 1
        }
        if result.contains("d") {
            resultStr.append(" d)")
            resultInt = resultInt + 1
        }
        if resultInt == 4 {
            resultStr.append(" e)")
            resultInt = 5
        }
        print("password \(password) - \(resultInt)\(resultStr)")
    }
    
    func search(letters: String) {
        let arr = ["lada", "sedan", "baklazhan"]
        let resultArr = arr.filter { $0.contains(letters) }
        print(resultArr)
    }
    
    func addPoint(number: String) {
        var numberForPoint = number
        if numberForPoint.count >= 4 {
            numberForPoint.insert(".", at: numberForPoint.index(numberForPoint.endIndex, offsetBy: -3))
            if numberForPoint.count >= 8 {
                numberForPoint.insert(".", at: numberForPoint.index(numberForPoint.endIndex, offsetBy: -7))
                if numberForPoint.count >= 12 {
                    numberForPoint.insert(".", at: numberForPoint.index(numberForPoint.endIndex, offsetBy: -11))
                    if numberForPoint.count >= 16 {
                        numberForPoint.insert(".", at: numberForPoint.index(numberForPoint.endIndex, offsetBy: -15))
                        if numberForPoint.count >= 20 {
                            numberForPoint.insert(".", at: numberForPoint.index(numberForPoint.endIndex, offsetBy: -19))
                            if numberForPoint.count >= 24 {
                                numberForPoint.insert(".", at: numberForPoint.index(numberForPoint.endIndex, offsetBy: -23))
                                if numberForPoint.count >= 28 {
                                    numberForPoint.insert(".", at: numberForPoint.index(numberForPoint.endIndex, offsetBy: -27))
                                    if numberForPoint.count >= 32 {
                                        numberForPoint.insert(".", at: numberForPoint.index(numberForPoint.endIndex, offsetBy: -31))
                                    }else {
                                        print("error")
                                    }}}}}}}}
        print(numberForPoint)
    }
    
    func reverce(startWord: String) {
        var arrChar = [Character]()
        var os = startWord
        for _ in 0..<os.count {
            let lastLetter = os.removeLast()
            arrChar.append(lastLetter)
        }
        //        print(arrChar)
        let reverceWord = String(arrChar)
        print(reverceWord)
    }
    
    func splitName() {
        let fullName = "NikitaNechiporenko"
        let name = fullName as NSString
        let range1 = NSMakeRange(0, 6)
        var str1 = name.substring(with: range1)
        print(str1)
        let range2 = NSMakeRange(6, 12)
        let str2 = name.substring(with: range2)
        print(str2)
        str1.append(" ")
        let str3 = str1 + str2
        print(str3)
    }
    
    func checkIchNa() {
        let patronymic = "Victorovich"
        if patronymic.hasSuffix("ich") {
            print("my middle name contains the ending ich")
        } else {
            print("my middle name does not contain an ending ich")
        }
    }
    
    func quantitySimbolsOfName() {
        let name = "Nikta"
        print(name.count)
    }
}

